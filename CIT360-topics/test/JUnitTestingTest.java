import org.junit.Test;

import static org.junit.Assert.*;

public class JUnitTestingTest {
    JUnitTesting test1 = new JUnitTesting();

    // Global variables
    int a;
    int b;

    // Positive test
    @Test
    public void cal1() {
        a = 2;
        b = 2;

        // Parameters
        int expected = a * b;
        int result = test1.junits(a, b);
        // Test
        assertEquals(expected, result);
    }

    //Negative test
    @Test
    public void cal2() {
        a = 4;
        b = 4;

        // Parameters
        int expected = a + b;
        int result = test1.junits(a, b);
        // Test
        assertEquals(expected, result);
    }

    // Positive test
    @Test
    public void fullName() {
        String a = "Jose ";
        String b = "Covarrubias";

        // Parameters
        String expected = a.concat(b);
        String result = test1.junitsText(a,b);
        // Test
        assertEquals(expected, result);
    }

    // Positive test
    @Test
    public void trueOrfalseTest() {
        a = 1;

        // Parameters
        boolean result = test1.trueOrfalse(a);
        // Test
        assertTrue(result);

    }
}