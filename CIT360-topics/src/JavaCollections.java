import java.awt.desktop.SystemSleepEvent;
import java.util.*;

public class JavaCollections {

    public static void Collections() throws InterruptedException {

        System.out.println("Java Collection Class on Java collection file.");

//================== REGULAR ARRAY ============================================
        System.out.println("\n===============================================================================" +
                "\n========== A regular array in Java =====================");

        // Array declaration
        int[] regularArray;

        // Allocating number of index spaces for the array
        regularArray = new int[5];

        // Array index values
        regularArray[0] = 101;
        regularArray[1] = 103;
        regularArray[2] = 105;
        regularArray[3] = 107;
        regularArray[4] = 109;

        // Loop to print each index of the array
        for (int i : regularArray) {
            // This makes the execution slow 1 sec each iteration
            Thread.sleep(200);
            System.out.println(i);
        }

//=================== COLLECTION LIST =========================================
        System.out.println("\n===============================================================================" +
                "\n========== Collection LIST ======================= ");

        // LIST can accept duplicates on the data stored
        List myList = new ArrayList();

        myList.add("Viper");
        myList.add("F-150");
        myList.add("Ram");
        myList.add("Mustang");
        myList.add("Escalade");

        System.out.println(myList);

        Thread.sleep(200);
        System.out.println("We can use the COMPARATOR function and the SORT method to arrange the list");

        Thread.sleep(200);
        Comparator sortMyList = null;
        myList.sort(sortMyList);
        System.out.println(myList);

//=================== COLLECTION SET ===========================================
        Thread.sleep(200);
        System.out.println("\n===============================================================================" +
                "\n=========== Collection SET =============================");

        // The caveat with SET is that it can store different types of data, but it
        // doesn't take duplicates.
        Set mySet = new HashSet();
        mySet.add(4);
        mySet.add("bike");
        mySet.add(3.4);
        mySet.add("Collection SET");

        // Print the SET out
        System.out.println("This is my SET: " + mySet);

        Thread.sleep(200);
        // We can use some of the methods of collections. The size of the SET
        System.out.println("What are the number of indexes in this SET? " + mySet.size());

        Thread.sleep(200);
        // We can use an Iterator, which an interface, to loop through the SET
        // The WHILE LOOP will not work without the Iterator.
        Iterator mySetIterator = mySet.iterator();
        while (mySetIterator.hasNext()) {
            Thread.sleep(200);
            System.out.println("My SET has a " + mySetIterator.next());
        }

        //Here are some methods that can be used with most collection interfaces.
        System.out.println();
        Thread.sleep(200);
        System.out.println("Does this collection SET has 4 values? " + mySet.contains(4));
        System.out.println("Is this collection SET empty? " + mySet.isEmpty());
        Thread.sleep(200);
        System.out.println("Was the bike removed from this SET? " + mySet.remove("bike"));
        System.out.println(mySet);

//============= COLLECTION TREESET ============================================
        /* The TREESET is a collection that can sort the array for you. data has
           to be of the same type. Duplicates will not show.
        */
        System.out.println("\n===============================================================================" +
                "\n================ NOW USING THE TREESET =================");
        TreeSet sortItForMe = new TreeSet();
        sortItForMe.add(305);
        sortItForMe.add(303);
        sortItForMe.add(303); //this item will not show.
        sortItForMe.add(305); //this item will not show.
        sortItForMe.add(208);
        System.out.println(sortItForMe);
        Thread.sleep(200);

//=============================================================================
        /*          COLLECTION MAP
        Map collections can function well creating list/arrays
        that can be used in phone directories or other data lists that
        need 2 sets of information. However, it does not take duplicate key
        values, but it does on the second parameter value.
        */
        System.out.println("\n===============================================================================" +
                "\n================ NOW USING THE MAP =================");
        Map raceCarNum = new HashMap();
        raceCarNum.put("blue", "1");
        raceCarNum.put("red", "5");
        raceCarNum.put("black", "3");
        raceCarNum.put("pink", "8");   //example of duplicate value for the
        raceCarNum.put("gray", "8");   //second parameter. This one is
        raceCarNum.put("neon", "8");   //allowed, but not the key value.
        System.out.println("Cars and their Numbers");
        Thread.sleep(200);
        System.out.println(raceCarNum);
        //it only prints the key/first value
        System.out.println("Using keySet()..." + raceCarNum.keySet());
        Thread.sleep(200);
        //it prints both the key/1st value and the value/2nd value
        System.out.println("Using entrySet()..." + raceCarNum.entrySet());
        Thread.sleep(200);
        //this method is used to return the value of an specified key
        System.out.println("The GET method for the key red = "
                + raceCarNum.get("red"));

//=============================================================================
        /*        COLLECTION QUEUE
        Queue collections have an interesting function, I will quote from
        www.geeksforgeeks.org "Typically order elements in FIFO
        (first-in-first-out) order except exceptions like PriorityQueue."
        */
        System.out.println("\n=================================================================================" +
                "\n============= NOW USING QUEUE ====================");
        Queue<String> myQueue = new LinkedList<>();
        myQueue.add("one");
        myQueue.add("two");
        myQueue.add("three");
        myQueue.add("2");
        Thread.sleep(200);
        System.out.println("My QUEUE has the following strings = "
                + myQueue + "\n");
        //this returns the number of elements on the queue using size() method.
        System.out.println(myQueue.size()
                + " is the size of this QUEUE using the method size().");
        //this returns the first/head of the queue using the peek() method.
        System.out.println("The method PEEK() in QUEUE [" + myQueue.peek() + "]");

        int j = 0;
        //this WHILE loop will remove the head/first item in the QUEUE until
        //there is not one left.
        while (j < myQueue.size()) {
            System.out.println("I just removed "
                    + myQueue.remove()// removes the value
                    + " from the queue.");
            Thread.sleep(200);
        }
        System.out.println("My QUEUE now contains = " + myQueue);

//=============================================================================
        System.out.println("\n=================================================================================");
        System.out.println("===== Examples of Exceptions or Error Handling ==================================");

//        String answer = "y";
//        String yesNo;
//        int itemN = 1;
//
//        List shoppingList = new ArrayList();
//
//        System.out.println("Let's add items to the shopping list: ");
//
//        Scanner userInput = new Scanner(System.in);
//
//        do {
//            String item = userInput.nextLine();
//            shoppingList.add(item);
//            System.out.println("add more items? (y/n)");
//                Scanner userAnswer = new Scanner(System.in);
//                yesNo = userAnswer.nextLine();
//        } while (answer.equals(yesNo));
//        System.out.println("Here is your shopping list: ");
//        for (int i = 0; i < shoppingList.size(); i++){
//            System.out.println(itemN++ + ".-" + shoppingList.get(i));
//        }

        //New List
        List anyList = new ArrayList();

        anyList.add("Car");
        anyList.add("Truck");
        anyList.add("Motorcycle");

        System.out.println("Here is your list");
        System.out.println(anyList);
        System.out.println("Print a single item by typing a number, try 4");
        try {
            Scanner scan1 = new Scanner(System.in);
            int scanNum = scan1.nextInt();
            System.out.println(anyList.get(scanNum));
        }catch (Exception e){
            System.out.println("The list is not that long, try typing a number from 0-2");
            Scanner scan2 = new Scanner(System.in);
            int scanNumber = scan2.nextInt();
            System.out.println(anyList.get(scanNumber));
        }

    }
}
