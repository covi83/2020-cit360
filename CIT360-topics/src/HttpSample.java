import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpSample {

    public static void ConnectHttp(){
        System.out.println(HttpSample.UseHTTP());
    }

    public static String UseHTTP(){

        String resultFromHttp = null;
        try {
            // Create the URL object
            URL myUrl = new URL("http://lcnnetwork.com/Other_pages/myText.txt");

            // Establish the connection
            HttpURLConnection myHttp = (HttpURLConnection) myUrl.openConnection();

            // Read the data coming from the website
            BufferedReader myReader = new BufferedReader(new InputStreamReader(myHttp.getInputStream()));

            StringBuilder myString = new StringBuilder();

            String myVar = null;
            while((myVar = myReader.readLine()) != null){
                myString.append(myVar + "\n");
            }
            resultFromHttp = myString.toString();

        }catch (IOException e) {
            System.out.println("Error connecting to the server.");
        }
        return resultFromHttp;
    }
}
