import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class JSONsample {
    public static void JSONsample1(){

        // try and catch error handling
        try {
            /* ObjectMapper is the main actor class of Jackson library. ObjectMapper class ObjectMapper
            provides functionality for reading and writing JSON, either to and from basic POJOs
            (Plain Old Java Objects), or to and from a general-purpose JSON Tree Model (JsonNode)"
            www.tutorialspoint.com */
            ObjectMapper mapJSON = new ObjectMapper();

            // collection MAP
            Map<String,Object> carInfo = new HashMap<>();

            // new Object class
            CarStats newCar = new CarStats();

            // Scanner object
            Scanner scan = new Scanner(System.in);

            // input from user
            // setting the 2 values of this object
            System.out.println("Type the name of your favorite car:");
            String inputModel = scan.nextLine();
            newCar.setModel(inputModel);
            System.out.println("What speed is this car rated for?");
            int inputSpeed = scan.nextInt();
            newCar.setSpeed(inputSpeed);

            // array
            System.out.println("Type number of cylinders?");
            int inputCylinder = scan.nextInt();
            System.out.println("Transmission speeds?");
            int inputTrans = scan.nextInt();
            int[] specs = {inputCylinder, inputTrans};

            // the following PUTs add the values on the MAP collection
            // JAVA Object
            carInfo.put("car", newCar);
            // JAVA String
            System.out.println("Who makes this car?");
            String inputMake = scan.next();
            carInfo.put("make", inputMake);
            // JAVA Boolean based on user input
            if(inputSpeed > 160){
                carInfo.put("fast", Boolean.TRUE);
            }else{
                carInfo.put("fast", Boolean.FALSE);
            }
            // we map the array
            carInfo.put("specs", specs);

            // to show the object before it's parsed, this doesn't show the specs array.
            System.out.println("Print carInfo " + carInfo);

            // we use the object mapper to write the values to a file,
            // passing the carInfo MAP as parameter.
            mapJSON.writeValue(new File("myFile.json"), carInfo);

            // read the file holding the object
            carInfo = mapJSON.readValue(new File("myFile.json"), Map.class);

            // to show the object once it's parsed
            System.out.println("Print carInfo after mapJSON " + carInfo);

            // we print out the values based on the KEY in the MAP
            System.out.println("======  Results ====================================");
            System.out.println(carInfo.get("car"));
            System.out.println("Make: " + carInfo.get("make"));
            System.out.println("Is this car fast? " + carInfo.get("fast"));
            System.out.println("Cylinders and speeds, respectively : " + carInfo.get("specs"));
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InputMismatchException e){
            System.out.println("The wrong datatype was entered. Try again. " + e);
        }
    }
}

class CarStats {
    private String model;
    private int speed;
    public CarStats(){}

    //Getters and setters
    public String getName() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    public String toString(){
        return "Car [ name: " + model + ", speed: " + speed + " ]";
    }
}




