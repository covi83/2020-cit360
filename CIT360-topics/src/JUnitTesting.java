public class JUnitTesting {

    // Global variables
    static int result;
    static String text;

    // Method to multiply 2 integers
    public static int junits(int a, int b){

       result =  a * b;
        return result;
    }

    // Method to concatenate 2 strings of text
    public static String junitsText(String a, String b){
        text = a.concat(b);
        return text;
    }

    public static boolean trueOrfalse(int a){
        System.out.println("Is one always greater than zero?");
        boolean yes = a > 0;
        return yes;
    }
}
