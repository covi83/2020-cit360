
/**
 *
 * @author josecovarrubias
 *
 *  One thing to note about Java threads is that you can only extend one
 *  class on a project. Also, anytime you use the  "extends Thread" class
 *  all of the new instances created will have the same value, meaning that
 *  the thread constructor will repeat once the first task has finished.
 */
public class PlayingWithThreads extends Thread{
    private int counter = 0;

    @Override
    public void run(){

        System.out.println("Using the Thread class. Counter: " + getName());

        for (int i = 0; i < 3; i++) {
            counter++;
            System.out.println("Using the Thread. Counter: "+ counter);
        }
    }
}
