
public class JavaMain {
    public static void main(String[] args) throws InterruptedException {

        System.out.println("**** Welcome to CIT 360 ****" +
                "\nHere are the topics we are learning about this semester." +
                "\n1.-Collections" +
                "\n2.-JSON" +
                "\n3.-URL and HTTP" +
                "\n4.-JUnit" +
                "\n5.-Servlet" +
                "\n6.-Threading" +
                "\n7.-Hibernate" +
                "\n" +
                "\nChoose one of the topics by typing the name to see it working.\n");

        // comment out this line once all cases are created
//        JavaCollections.Collections();
//        JSONsample.JSONsample1();
//        HttpSample.ConnectHttp();
          PlayingRunnables.PlayingRunnablesandThreads();

//=============================================================================
// Uncomment all the lines below once each topic is developed


//        //      user input to select a topic
//        Scanner userInput = new Scanner(System.in);
//        String userChoice = userInput.nextLine().toUpperCase();
//
//
//        //        Switch logic to select topic to learn more about.
//        switch (userChoice){
//            case "COLLECTIONS":
//                System.out.println("collections case");
//                  JavaCollections.Collections();
//                break;
//            case "JSON":
//                System.out.println("json case");
//                 JSONsample.JSONsample();
//                break;
//            case "URL AND HTTP":
//                System.out.println("URL case");
//                  HttpSample.ConnectHttp();
//                break;
//            case "JUNIT":
//                System.out.println("junit case");
//                break;
//            case "SERVLET":
//                System.out.println("servlet case");
//                break;
//            case "THREADING":
//                System.out.println("threading case");
//                break;
//            case "HIBERNATE":
//                System.out.println("hibernate case");
//                break;
//        }

    }
}
